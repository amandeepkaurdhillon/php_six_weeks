<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <form action="">
                    <div id="msz"></div>
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" id="fullname">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password">
                    </div>
                    <div class="form-group">
                        <input type="button" class="btn btn-success" value="Sign Up" onclick="register()">
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Sr. No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <!-- <th>Password</th> -->
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="table_data"></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="editmodel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>EDIT RECORD</h2>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" id="editname">
                        <input type="hidden" class="form-control" id="editid">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="editemail">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success" onclick="update_record()">Update Record</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    </div>
    <script>
    function register(){
        var name = document.getElementById("fullname").value;
        var email = document.getElementById("email").value;
        var pass = document.getElementById("password").value;

        $.ajax({
            url:"ajax_database.php",
            type:"post",
            data:{fullname:name, email:email, password:pass},
            success:function(data){
                // alert(data);
                $("#msz").html(data).addClass("alert alert-success");
                get_data();
            }
        });
    }

    function get_data(){
        $.ajax({
            url:"ajax_database.php",
            type:"post",
            data:{getdata:""},
            success:function(data){
                $("#table_data").html(data);
            }
        })
    }

    function delete_data(id){
        var con = confirm("Are you sure want to delete?");
        if(con){
            $.ajax({
                url:"ajax_database.php",
                type:"post",
                data:{delete_id:id},
                success:function(data){
                    alert(data);
                    get_data();
                }
            });
      
    }
}

function editdata(id){
    $.ajax({
        url:"ajax_database.php",
        type:"post",
        data:{single_data:id},
        success:function(data){
            $("#editname").val(data.name);
            $("#editemail").val(data.email);
            $("#editid").val(data.id);
            get_data();
        }
    });
}

function update_record(){
    var new_name = document.getElementById("editname").value;
    var new_email = document.getElementById("editemail").value;
    var id = document.getElementById("editid").value;
    
    $.ajax({
        url:"ajax_database.php",
        type:"post",
        data:{newname:new_name, newemail:new_email,id:id},
        success:function(data){
            alert(data);
            $(".close").click();
            get_data();
        }
    })
}
    get_data();
    </script>
</body>
</html>