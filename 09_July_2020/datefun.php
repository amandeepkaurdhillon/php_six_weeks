<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Date Function</title>
</head>
<body>
    <?php
        echo "Day is: ".date("d-m-Y")."<br/>";
        echo "Day is: ".date("d/m/Y")."<br/>";
        echo "Day is: ".date("Y-m-d")."<br/>";
        echo "Day is: ".date("l")."<br/>";
    ?>
</body>
</html>