<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Functions</title>
</head>
<body>
    <!-- Types of functions
    1. Built In functions 
    2. User defined functions -->

    <?php
        // Non parametrized functions
        function greet(){ //defination
            echo "<h1>Good Afternoon All, How are you??</h1>"; //function body
        }

        // greet(); //calling
        // greet();
        // greet();

        //Parametrized function
        function student($name,$age,$color){
           echo "<h1 style='color:".$color."'>".$name." is ".$age." years old</h1>";
        }

        student("Harry",5,"red");
        student("Peter Parker",22,"green");
        student("Jenny",18,"magenta");
        student("Schinchan",5,"orange");

        function square($x){
            return $x*$x."<br/>";
        }
        echo square(2);
        echo square(8);
        echo square(87654);
        echo square(16);
    ?>
</body>
</html>