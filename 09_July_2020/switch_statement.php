<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Switch Statment</title>
</head>
<body>
    <?php
        $day = date("l"); //Built in function to get week day
        echo "<h1>".$day."</h1>";

        switch($day){
            case "Sunday":
                echo "Have a great holiday";
                break;

            case "Monday":
                echo "Have a great day!!!";
                break;

            case "Tuesday":
                echo "Keep Up great work!!!";
                break;

            case "Thursday":
                echo "Today is Thursday!!!";
                break;

            default:
                echo "No case is true";
        }
    ?>
</body>
</html>