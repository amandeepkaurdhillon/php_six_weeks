<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Handling</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto p-4" style="box-shadow:0px 0px 10px gray;">
                <div class="alert alert-success">
                    <?php
                        
                        if(isset($_POST["fullname"])){
                            $name = $_POST["fullname"];

                            if(empty($name)){
                                echo "<p class='text-danger'>Name is required!!!</p>";
                            }
                            $email = $_POST["email"];
                            $pass = md5($_POST["password"]); //Encrypted/Secure Password
                            $gen = $_POST["gender"];

                            echo "Registration Success!!!";
                            echo "<h4>Name: ".$name."</h4>";
                            echo "<p>Email: ".$email."</p>";
                            echo "<p>Password: ".$pass."</p>";
                            echo "<p>Gender: ".$gen."</p>";
                    }
                    ?>
                </div>
                <h4>REGISTER HERE</h4>
                <form action="" method="post">
                    <div class="form-group">
                        <label>Name<sup class="text-danger font-weight-bold">*</sup>  </label>
                        <input type="text" class="form-control" name="fullname" placeholder="Enter Name..." required>
                    </div>
                    <div class="form-group">
                        <label>Email<sup class="text-danger font-weight-bold">*</sup></label>
                        <input type="email" class="form-control" name="email" placeholder="Enter Email..." required>
                    </div>
                    <div class="form-group">
                        <label>Password<sup class="text-danger font-weight-bold">*</sup></label>
                        <input type="password" class="form-control" name="password" placeholder="Enter Password..." required>
                    </div>
                    <div class="form-group">
                        <label>Gender</label><sup class="text-danger font-weight-bold">*</sup> <br>
                        <input type="radio" value="male" name="gender" required> <label> Male</label>
                        <input type="radio" value="female" name="gender" required> <label> Female</label>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Register" class="btn btn-success">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>