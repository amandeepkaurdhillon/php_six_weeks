<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP OOP</title>
</head>
<body>
    <?php
        //Class
        class Student{
            public $clz = "XYZ Engg. College";

            function intro(){
                echo "<h1>I am a member function</h1>";
            }
        }

        //Object
        $st1 = new Student();
        
        //Access Data members
        echo $st1->clz;

        //Access member functions
        $st1->intro();

?>
</body>
</html>