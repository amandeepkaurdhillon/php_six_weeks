<?php
    require "connect.php";
    
    //get record
    $id = $_GET["id"];
    $qry = "SELECT * FROM register WHERE id='$id'";
    $result = $con->query($qry);
    $data = $result->fetch_assoc();

    if(isset($_POST["fullname"])){
    //Update record
        $name = $_POST["fullname"];
        $email = $_POST["email"];
        $password = md5($_POST["password"]);

        $qry = "UPDATE register SET name='$name', email='$email', password='$password' WHERE id='$id'";
        if($con->query($qry)){
            echo "<h1>Updated Successfully</h1>";
            header("Location:all_users.php");
        }else{
            echo "<h1>Could not update!!!</h1>";
        }
}

    include "header.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto mt-5 p-4" style="box-shadow:0px 0px 10px black;">
                <form method="post">
                <h2>UPDATE RECORD</h2>
                    <div class="form-group">
                        <label>User ID</label>
                        <input type="text" class="form-control" value="<?php echo $data['id']; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" value="<?php echo $data['name']; ?>" name="fullname">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" value="<?php echo $data['email']; ?>" name="email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" value="<?php echo $data['password']; ?>" name="password">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Update">
                    </div>
                </form>            
            </div>
        </div
    </div>    
</body>
</html>
