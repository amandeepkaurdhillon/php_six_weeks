<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Users</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <!-- LINK FOR ICONS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php include 'header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
                <table class="table table-bordered table-striped table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //DATABSE CONNECTION
                            $con = new mysqli('localhost','root','','php_batch');
                            if($con->connect_error){
                                echo "COULD NOT CONNECT TO DATABASE";
                            }
                        //RETRIEVE DATA
                            $qry = "SELECT * FROM register";
                            // $qry = "SELECT * FROM register WHERE id=2";
                            $result = $con->query($qry);
                            
                            while($row=$result->fetch_assoc()){
                                
                                echo "<tr>";
                                echo "<td>".$row["id"]."</td>";
                                echo "<td>".$row["name"]."</td>";
                                echo "<td>".$row["email"]."</td>";
                                echo "<td>";
                                echo "<a href='update.php?id=".$row['id']."' class='btn btn-success btn-sm mr-2'><i class='fa fa-edit'></i> Edit</a>";
                                echo "<a href='delete.php?id=".$row['id']."' class='btn btn-danger btn-sm mr-2'><i class='fa fa-trash'></i> Delete</a>";
                                echo "</td>";
                                echo "</tr>";
                            }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</body>
</html>