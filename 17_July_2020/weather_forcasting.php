<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Weather Forcasting</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 mx-auto p-3 my-3"style="box-shadow:0px 0px 10px gray;">
                <form method="post">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Enter City Name..." name="city">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-outline-success btn-block" value="GET WEATHER">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
        <div class="row">
        <?php
        if(isset($_POST["city"])){
            $city = $_POST["city"];
            $api = "https://api.openweathermap.org/data/2.5/forecast?q=".$city."&appid=e79a7d07c4e3bf54dd170836c3e88496&units=metric";
            $data = file_get_contents($api);
            $ab = json_decode($data, true);

            echo "<div class='col-md-12 bg-danger text-center text-light'>";
            echo "<h1>".$ab["city"]["name"]." (".$ab["city"]["country"].")</h1>";
            echo "</div>";
            foreach($ab["list"] as $d){
                echo "<div class='col-md-3 text-center p-3'><div class='p-2' style='box-shadow:0px 0px 10px gray;'>";
                
                echo $d["dt_txt"]."<br/>";
                echo "<img src='http://openweathermap.org/img/wn/".$d["weather"][0]["icon"]."@2x.png'><br/>";
                echo $d["main"]["temp"]."&deg;C<br/>";
                echo "<em>".$d["weather"][0]["main"]." (".$d["weather"][0]["description"].")</em><hr/>";
                echo "</div></div>";
            }
        }
        ?>            
        </div>
    </div>    
</body>
</html>
