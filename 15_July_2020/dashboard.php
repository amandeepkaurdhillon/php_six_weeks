<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<?php 
session_start();
if(isset($_SESSION["login"])){
    if($_SESSION["login"]==false){    
     die("<p class='alert alert-danger'>Access Denied!! You are not allowed to view this page, Please login first</p>");

    }   
}

?>
<?php include "header.php" ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 bg-primary pt-5" style="height:600px;">
            <nav class="navbar navbar-dark">
                <a href="" class="navbar-brand">WEBSITE NAME</a>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="dashboard.php" class="nav-link">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a href="all_users.php" class="nav-link">All Users</a>
                    </li>
                    <li class="nav-item">
                        <a href="logout.php" class="nav-link">Logout</a>
                    </li>
                </ul>
                </nav>
            </div>
            <div class="col-md-10 mt-5">
                <div class="jumbotron">
                    <h1>WELCOME TO  DASHBOARD
                    <?php echo $_SESSION["email"] ?>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</body>
</html>