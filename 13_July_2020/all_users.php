<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All Users</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

</head>
<body>
<?php include 'header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
                <table class="table table-bordered table-striped table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //DATABSE CONNECTION
                            $con = new mysqli('localhost','root','','php_batch');
                            if($con->connect_error){
                                echo "COULD NOT CONNECT TO DATABASE";
                            }
                        //RETRIEVE DATA
                            $qry = "SELECT * FROM register";
                            // $qry = "SELECT * FROM register WHERE id=2";
                            $result = $con->query($qry);
                            
                            while($row=$result->fetch_assoc()){
                                
                                echo "<tr>";
                                echo "<td>".$row["id"]."</td>";
                                echo "<td>".$row["name"]."</td>";
                                echo "<td>".$row["email"]."</td>";
                                echo "</tr>";
                            }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</body>
</html>