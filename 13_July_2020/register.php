<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    
    <style>
    .left-side{
        background:url("rg.jpg");background-size:100% 100%;
    }
    </style>
</head>
<body>
    <?php include 'header.php' ?>    
    <div class="container my-5" style="box-shadow:0px 0px 10px gray;">
        <div class="row">
            <div class="col-md-6 left-side p-5"></div>
            <div class="col-md-6 p-5">
                <form action="" method="post">
                <h2>Fill Your Details</h2>
                <?php
                    if(isset($_POST["fullname"])){
                        $name = $_POST["fullname"];
                        $email = $_POST["email"];
                        $pass = md5($_POST["password"]);

                        //DATABASE CONNECTION
                        $con =new mysqli('localhost','root','','php_batch');
                        if($con->connect_error){
                            echo "COULD NOT CONNECT TO DATABASE".$connect_error;
                        }

                        //INSERT DATA
                        $abc = "INSERT INTO register(name,email,password) VALUES('$name','$email','$pass')";
                        if($con->query($abc)){
                            echo "<p class='alert alert-success'>".$name." registred successfully!!!</p>";
                        } else{
                            echo "<p class='alert alert-danger'>Could not register</p>";
                        }
                    }
                    ?>
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" placeholder="Enter Full Name..." name="fullname">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" placeholder="Enter Email Address..." name="email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Choose Password..." id="pass" name="password" onkeyup="check_password()">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" placeholder="Confirm Password ..." id="cpass" onkeyup="check_password()">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Register Now" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include 'footer.php' ?>  
    <script>
    function check_password(){
        var p = document.getElementById("pass").value;
        var cp = document.getElementById("cpass").value;

        if(p==cp){
            $("#pass").css({"border":"1px solid green"});
            $("#cpass").css({"border":"1px solid green"});
            $("#submit").removeAttr("disabled");
        }else{
            $("#pass").css({"border":"1px solid red"});
            $("#cpass").css({"border":"1px solid red"});
            $("#submit").attr("disabled",true);
        }
        

    }
    </script>
</body>
</html>