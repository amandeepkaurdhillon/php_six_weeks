<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Constructor & Destructor</title>
</head>
<body>
    <?php

        class circle{

            function __construct(){
                $this->PI = 3.14;
                echo "Value Initialized<br/>";
                // echo "I AM A CONSTRUCTOR I WILL CALL AUTOMATICALLY!<br>";
            }

            function Area($r){
                return $this->PI * $r * $r;
                // echo "<h1>THIS IS A MEMBER FUNCTION</h1>";
            }

            function __destruct(){
                echo "VAlues Destroyed!!!";
            }
        }

        $ob = new circle();
        echo $ob->Area(100)."<br/>";
        echo $ob->Area(10)."<br/>";
        echo $ob->Area(5)."<br/>";
       
      

?>
</body>
</html>