<?php

 class first{ //parent
        public $x = 10;
        protected $y = 20;

        function add(){
            return $this->x + $this->y;
        }
        
    }
    //Inheritance
    class second extends first{ //child
        public $a = 200;

        function sum(){
            // echo "HIII";
            return $this->x + $this->y + $this->a;
        }
    }

    //Multi level inheritance
    class third extends second{
        public $b  = 0.5;
        function mul(){
            return $this->x * $this->y * $this->a * $this->b;
        }
    }
    $obj = new first();
    // echo $obj->x."<br/>";
    // echo  $obj->add()."<br/>";


    $sec = new second();
    // echo $sec->sum();
    // echo $sec->a;

    $third = new third();
    echo $third->mul();
    

?>