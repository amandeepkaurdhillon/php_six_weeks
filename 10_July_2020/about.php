<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Page</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <?php include "header.php" ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="jumbotron">ABOUT WEBSITE</h1>
                <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad architecto delectus, vitae corporis eum possimus a omnis earum atque, explicabo nostrum voluptatum fugit consequuntur, asperiores excepturi cumque. Iure, facere nesciunt!
                </p>
            </div>
        </div>
    </div>
    <?php include "footer.php" ?>
</body>
</html>