<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Access Specifiers</title>
</head>
<body>
    <?php
        class Employee{
            public $company;
            private $emp_code = 10001;
            protected $name;

            public function info(){
                echo "<h1 style='color:red;'>WELCOME ".$this->emp_code." TO ".$this->company."</h1>";
            }
        }
        //Object

        $emp = new Employee();
        $emp->company = "ABCD Tech.";
        
        $emp->info();
        echo $emp->company;

        // $emp->emp_code=10001; // we can't access private properties outside the class
    ?>
</body>
</html>