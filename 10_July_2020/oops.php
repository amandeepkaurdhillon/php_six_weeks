<html> 
 <head>
   <title>PHP OOPS</title>
 </head>
 <body>
    <?php
      class student{
	  public $clz; //data member
		
	  function intro(){
		  echo "MEMBER FUNCTION";
	  }
      function profile($name,$roll_no){  //member function
		echo "<h2>Student ".$name." has ".$roll_no." roll no</h2>";
	 }        

	}

	//Creating object
	
	$st1 = new student();
	$st2 = new student();

	$st1->intro();

	//Set values
	$st1->clz = "XYZ Engg. College";
	$st2->clz = "<h1>ABCD College of management</h1>";

	echo $st2->clz;
	echo $st1->clz;

	$st1->profile("Aman",1001);
	$st2->profile("James",1002);
    ?>
 </body>
</html>
<!--
Access Specifiers
Public - can be used everywhere
private - only within the class 
protected - can be accessed by same class & its child classes
-->