<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multi dimentional</title>
</head>
<body>
    <?php
        $employees = [
            ["name"=>"Peter Parker","designation"=>"Software Engg.","salary"=>50000],
            ["name"=>"Jack","designation"=>"Business Manager","salary"=>70000],
            ["name"=>"Mr. Bean","designation"=>"Business Executive","salary"=>100000],
            ["name"=>"James","designation"=>"Business Executive","salary"=>100000],
        ];

        // echo "<pre>";
        // print_r($employees);
        // echo "</pre>";

        // echo ($employees[0]["name"]);
        // echo ($employees[1]["designation"]);

        foreach($employees as $x){
            echo $x["name"]."<br/>";
        }

?>
</body>
</html>