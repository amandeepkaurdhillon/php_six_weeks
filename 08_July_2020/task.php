<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employees</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
    <table class="table table-hover table-primary">
        <thead class="thead-dark">
            <tr>
                <th>Sr. No.</th>
                <th>Profile Picture</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Salary (Rs.)</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $employees = [
                ["name"=>"Peter Parker","designation"=>"Software Engg.","salary"=>50000, "profile"=>"https://www.bcjobs.ca/blog/wp-content/uploads/2019/04/12-ways-confident-professional-at-work.png"],
                ["name"=>"Jack","designation"=>"Business Manager","salary"=>70000, "profile"=>"https://www.bcjobs.ca/blog/wp-content/uploads/2019/04/12-ways-confident-professional-at-work.png"],
                ["name"=>"Mr. Bean","designation"=>"Business Executive","salary"=>100000, "profile"=>"https://www.bcjobs.ca/blog/wp-content/uploads/2019/04/12-ways-confident-professional-at-work.png"],
                ["name"=>"James","designation"=>"Business Executive","salary"=>100000, "profile"=>"https://www.bcjobs.ca/blog/wp-content/uploads/2019/04/12-ways-confident-professional-at-work.png"],
                ["name"=>"Schinchan","designation"=>"Business Executive","salary"=>100000, "profile"=>"https://www.bcjobs.ca/blog/wp-content/uploads/2019/04/12-ways-confident-professional-at-work.png"],
                ["name"=>"Potter","designation"=>"Business Executive","salary"=>100000, "profile"=>"https://www.bcjobs.ca/blog/wp-content/uploads/2019/04/12-ways-confident-professional-at-work.png"],
            ];
            $c=1;
            foreach($employees as $emp){
                echo "<tr>";
                echo "<td>".$c."</td>";
                echo "<td><img src='".$emp["profile"]."' style='height:100px;width:100px;border-radius:50%;'></td>";
                echo "<td>".$emp["name"]."</td>";
                echo "<td>".$emp["designation"]."</td>";
                echo "<td>&#8377;".$emp["salary"]."</td>";
                echo "</tr>";
                $c++;
            }
        ?>    
        </tbody>
    </table>
    
</body>
</html>