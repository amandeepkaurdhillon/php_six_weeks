<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Associative Array</title>
    <style>
    .abcd{
        width:50%;box-shadow:0px 0px 10px gray;margin:auto;text-align:center;
    }
    </style>
</head>
<body>
<div class="abcd">
    <?php
        $student = array("name"=>"Schinchan Nohara","age"=>5,"roll_no"=>1001,"country"=>"JP",
        "pic"=>"https://i.pinimg.com/originals/14/fc/03/14fc030a45875ea3021063e18d433ea5.png");
        // echo $student["name"];

        echo "<img src='".$student["pic"]."' style='height:200px;'>";
        
        foreach($student as $a=>$b){
            echo "<h1 style='color:red;'>".$a."</h1>";
            echo "<p style='color:green;'>".$b."</p><hr>";
        }

    ?>
</div>
</body>
</html>