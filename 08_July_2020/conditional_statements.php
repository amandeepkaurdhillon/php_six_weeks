<!-- if
if else 
if elseif else 
nested if  -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conditional Statements</title>
</head>
<body>
    <?php 
        $name = "admin";
        $password = "admin";
        // if($name=="Aman"){
        //     echo "<h1>WELCOME ".$name."</h1>";     
        // }

        // if($name=="Aman"){
        //     echo "<h1 style='color:green;'>WELCOME ".$name."!!!</h1>";     
        // }else{
        //     echo "<h1 style='color:red;'>Incorrect Name!!!</h1>";   
        // }

        if($name=="Aman" && $password==1234){
            echo "<h1 style='color:green;'>WELCOME TO MY SITE ".$name."</h1>";
        }
        else if($name=="admin" && $password=="admin"){
            echo "<h1 style='color:orange;'>WELCOME ADMIN!!!! You are a superuser!!!</h1>";
        }
        else{
            echo "<h1 style='color:red;'>Sorry You are not allowed Here!!!</h1>";
        }

?>
</body>
</html>