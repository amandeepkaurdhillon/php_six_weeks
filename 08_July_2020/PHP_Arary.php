<?php 

// Indexing
// Associative
// Multidimentional

$hobbies = ["Singing","Travelling","Music"];

// print_r($hobbies);
echo $hobbies[1]; 

// Associative array

$student = array("name"=>"Peter Parker","roll_no"=>1001,"class"=>"PHP");

// echo '<h1>'.$student["name"].'</h1>';
// echo $student["roll_no"];

//Looping
$cities = ["Mohali","Patiala","Chandigarh","Noida","Delhi","Shimla"];

array_push($cities,"Amritsar","Jalandhar");

echo "<select>";

foreach($cities as $val){
    echo "<option>".$val."</option>";
}

echo "<select><br/>";

// echo count($cities);
// $new= array_merge($cities,$hobbies);
// print_r($new)
?>